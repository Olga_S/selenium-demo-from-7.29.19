def fizzbuzz_recursion(start, end):
    if start % 15 == 0:
        print('FizzBuzz')
    elif start % 3 == 0:
        print('Fizz')
    elif start % 5 == 0:
        print('Buzz')
    else:
        print(start)
    if start < end:
        fizzbuzz_recursion(start + 1, end)


fizzbuzz_recursion(1, 100)